# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notice', '0002_register'),
    ]

    operations = [
        migrations.AddField(
            model_name='notice',
            name='contact',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='notice',
            name='contact_no',
            field=models.BigIntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='notice',
            name='image',
            field=models.URLField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='notice',
            name='venue',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='notice',
            name='deadline',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
