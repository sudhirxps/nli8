# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Notice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subject', models.CharField(max_length=100)),
                ('description', models.TextField(default=None)),
                ('registration', models.BooleanField(default=False)),
                ('deadline', models.DateTimeField()),
                ('department', models.ForeignKey(related_name='notice', to='auth.Group')),
                ('owner', models.ForeignKey(related_name='notice', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
