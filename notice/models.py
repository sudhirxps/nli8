from django.db import models

# Create your models here.

class Notice(models.Model):
	subject = models.CharField(max_length=100)
	description = models.TextField(default=None)
	registration = models.BooleanField(default=False)
	
	owner = models.ForeignKey('auth.User', related_name='notice')
	department = models.ForeignKey('auth.Group', related_name='notice')

	#Optional
	image = models.URLField(max_length=200,null=True,blank=True)
	deadline = models.DateTimeField(null=True,blank=True,)
	contact = models.CharField(max_length=100,null=True,blank=True)
	contact_no = models.BigIntegerField(null=True,blank=True)
	venue = models.CharField(max_length=100,null=True,blank=True)

	def __unicode__(self):
		return self.subject

class Register(models.Model):
	notice = models.ForeignKey(Notice)
	user = models.ForeignKey('auth.User',related_name='register')

	def __unicode__(self):
		return self.notice.subject + " " + self.user.username