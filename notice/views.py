from django.contrib.auth.models import User, Group
from rest_framework import viewsets,filters
from notice.serializers import UserSerializer, GroupSerializer, NoticeSerializer, RegisterSerializer
from notice.models import Notice,Register


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('groups',)

class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    filter_fields = ('name',)

class NoticeViewSet(viewsets.ModelViewSet):

    queryset = Notice.objects.all()
    serializer_class = NoticeSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('department',)

class RegisterViewSet(viewsets.ModelViewSet):

    queryset = Register.objects.all()
    serializer_class = RegisterSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('user',)
