from django.contrib.auth.models import User, Group
from rest_framework import serializers
from notice.models import Notice,Register
from rest_framework import permissions

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups','register')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class NoticeSerializer(serializers.HyperlinkedModelSerializer):
	
	class Meta:
		model = Notice
		fields = ('url','subject','description',
			'owner','department','registration',
			'deadline','contact','contact_no','venue','image',
			)

class RegisterSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Register
		fields = ('url','notice','user')