from django.contrib import admin

from notice import models

# Register your models here.

admin.site.register(models.Notice)
admin.site.register(models.Register)